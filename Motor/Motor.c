/*
 * Motor.c
 *
 * Created: 4/4/2016 8:24:43 PM
 *  Author: reed
 */ 

#include "SDO.h"
#include "general.h"
#include "assert.h"
#include "can.h"

void Init(int motorID) {
	SDO_packet ClearErrors = {motorID, 0x3000, 0x00, 0x01};
	SDO_packet SetMotor = {motorID, 0x3900, 0x00, 0x01};
	SDO_packet SetPoles = {motorID, 0x3910, 0x00, 10};
	SDO_packet MotorPolarity = {motorID, 0x3911, 0x00, 0x01};
	SDO_packet PowerEnable = {motorID, 0x3004, 0x00, 0x01};
	
	SDOWritePacket(ClearErrors);
	_delay_ms(100);
	SDOWritePacket(SetMotor);
	_delay_ms(100);
	SDOWritePacket(SetPoles);
	_delay_ms(100);
	SDOWritePacket(MotorPolarity);
	_delay_ms(100); 
	SDOWritePacket(PowerEnable);
	_delay_ms(100);
	
}

void VelMode(int motorID) {
	SDO_packet ModeVel	 = {motorID, 0x3003, 0x00, 0x3};
	
	SDOWritePacket(ModeVel);
	_delay_ms(100);
}

void PosMode(int motorID) {
	SDO_packet ModePos = {motorID, 0x3003, 0x00, 0x7};
	SDO_packet SetPositionWindow = {motorID, 0x3732, 0x00, 1000};
	SDO_packet DesiredVelocity1000 = {motorID, 0x3300, 0x0, 1000};
	SDO_packet ResetPosition = {motorID, 0x3762, 0x00, 0x00};
		
	SDOWritePacket(ModePos);
	_delay_ms(10);
	SDOWritePacket(DesiredVelocity1000);
	_delay_ms(10);
	SDOWritePacket(SetPositionWindow);
	_delay_ms(10);
	SDOWritePacket(ResetPosition);
}

void SetVel(int motorID, int Vel) {
	SDO_packet DesiredVelocity = {motorID, 0x3300, 0x0, Vel};
		
	SDOWritePacket(DesiredVelocity); 
}

void MoveCounts(int motorID, int Counts) {
	SDO_packet Move = {motorID, 0x3791, 0x00, Counts};
	
	SDOWritePacket(Move);
}