/*
 * IncFile1.h
 *
 * Created: 4/4/2016 8:24:18 PM
 *  Author: reed
 */ 


#ifndef Motor_H_
#define Motor_H_

void Init(int motorID); 
void VelMode(int motorID);
void PosMode(int motorID);
void SetVel(int motorID, int Vel);
void MoveCounts(int motorID, int Counts);


#endif /* Motor_H_ */