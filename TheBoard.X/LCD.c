
#include <p33EP512GP504.h>
#include "timer.h"
#include "LCD.h"

#define DB0         LATBbits.LATB15
#define DB1         LATBbits.LATB14
#define DB2         LATAbits.LATA7
#define DB3         LATAbits.LATA10
#define DB4         LATBbits.LATB13
#define DB5         LATBbits.LATB12
#define DB6         LATBbits.LATB11
#define DB7         LATBbits.LATB10
#define EnablePin   LATAbits.LATA0
#define RegSelect   LATBbits.LATB0
#define ReadWrite   LATAbits.LATA1

#define putByte(d)  writeByte((d),1)
#define LCDcmd(c)   writeByte(c,0)

#define CursorHome() writeByte(2,0)

//CONFIG 4: Display ON/OFF control 
//0b00001xxx 
#define CONFIG4         0x08
#define DISPLAYOn       0x04
#define CursorOn        0x02
#define CursorBlinkOn   0x01

//CONFIG 6: Function set 
//0b001xxxxx
#define CONFIG6         0x20
#define BusMode8        0x10
#define BusMode4        0x00
#define LineMode1       0x00  //only on line of the lcd will be used
#define LineMode2       0x08
#define Format_5x11     0x04

//CONFIG 8: Function set 
//0b1xxxxxxx
#define CONFIG8 0x80
#define Line1Home   0x00
#define Line2Home   0xC0

#define OUTPUT 0
#define HIGH   1
#define LOW    0 


void pulseEnable(void);

void initDatabus();
void demoLCD()
{
    char LCDchar[16];
    initLCD();
    sprintf(LCDchar,"Welcome %d ", 2);
    writeMsg(1,LCDchar);
    writeMsg(2,"Embedded systems");
}
void initLCD()
{
    initDatabus();
    ReadWrite = LOW;    //We will not be Reading so this should always be low
    LCDcmd(CONFIG4 | DISPLAYOn | CursorOn | CursorBlinkOn);
    __delay_ms(1);
    LCDcmd(CONFIG6 | BusMode8 | LineMode2 | Format_5x11);
    __delay_ms(1);
    ClrLCD();
    __delay_ms(2);
    LCDcmd(0b00000110);     //Moves cursor right and DDram address is increased by 1  
    __delay_ms(1);
}
void initDatabus()
{
    TRISBbits.TRISB15 = OUTPUT;     //DB0 LCD pin Init
    TRISBbits.TRISB14 = OUTPUT;     //DB1 LCD pin Init
    TRISAbits.TRISA7  = OUTPUT;     //DB2 LCD pin Init
    TRISAbits.TRISA10 = OUTPUT;     //DB3 LCD pin Init
    TRISBbits.TRISB13 = OUTPUT;     //DB4 LCD pin Init
    TRISBbits.TRISB12 = OUTPUT;     //DB5 LCD pin Init
    TRISBbits.TRISB11 = OUTPUT;     //DB6 LCD pin Init
    TRISBbits.TRISB10 = OUTPUT;     //DB7 LCD pin Init
    TRISAbits.TRISA0  = OUTPUT;     //EnablePin init to output
    TRISBbits.TRISB0  = OUTPUT;     //Register Selact pin init to output
    TRISAbits.TRISA1  = OUTPUT;     //R/W pin init to output
    __delay_ms(1);

}
void writeMsg(int line,char *msg)
{
    if(line <= 1)
    {
        LCDcmd(CONFIG8 | Line1Home);
    }
    else
    {
        LCDcmd(CONFIG8 | Line2Home);
    }
    __delay_ms(1);
    while(*msg)putByte(*msg++);
}
void writeByte(char _dataByte,int rs)
{
    RegSelect = rs;
    DB0 = ((_dataByte >> 0) & 0x01);
    DB1 = ((_dataByte >> 1) & 0x01);
    DB2 = ((_dataByte >> 2) & 0x01);
    DB3 = ((_dataByte >> 3) & 0x01);
    DB4 = ((_dataByte >> 4) & 0x01);
    DB5 = ((_dataByte >> 5) & 0x01);
    DB6 = ((_dataByte >> 6) & 0x01);
    DB7 = ((_dataByte >> 7) & 0x01);
    pulseEnable();
}
void pulseEnable(void)
{
    EnablePin = LOW;
    __delay_us(10);
    EnablePin = HIGH;
    __delay_us(10);
    EnablePin = LOW;
    __delay_us(100);
    
}