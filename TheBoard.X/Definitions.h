/* 
 * File:   Definitions.h
 * Author: John
 *
 * Created on October 24, 2017, 5:40 PM
 */

#ifndef DEFINITIONS_H
#define	DEFINITIONS_H

#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include "timer.h"
#include "../Common/CommsDefenition.h"

//*********LED INIT BITS***********
#define pinMODE_LED1 TRISBbits.TRISB8
#define pinMODE_LED2 TRISBbits.TRISB7
#define pinMODE_LED3 TRISBbits.TRISB6
#define pinMODE_LED4 TRISBbits.TRISB5
#define pinMODE_LED5 TRISCbits.TRISC3
#define pinMODE_LED6 TRISAbits.TRISA9
#define pinMODE_LED7 TRISBbits.TRISB4
#define pinMODE_LED8 TRISAbits.TRISA8 
//*********************************
#define LED1 LATBbits.LATB8
#define LED2 LATBbits.LATB7
#define LED3 LATBbits.LATB6
#define LED4 LATBbits.LATB5
#define LED5 LATCbits.LATC3
#define LED6 LATAbits.LATA9
#define LED7 LATBbits.LATB4
#define LED8 LATAbits.LATA8
//*********************************
#define ON  0
#define OFF 1
#define OUTPUT  0
#define INPUT 1
//*********************************


#define GENERAL_PIN8_INTERRUPT
//#define GENERAL_PIN7_INTERRUPT
//#define GENERAL_PIN6_INTERRUPT
//#define GENERAL_PIN5_INTERRUPT
//#define GENERAL_PIN4_INTERRUPT



#define GLOBAL_INTERRUPTS  INTCON2bits.GIE
#define FastTransferAddress 0x04
#endif	/* DEFINITIONS_H */

