#include <p33EP512GP504.h>
#include "Definitions.h"

#include "I2C.h"
void I2C1Stop();
void I2C1Start();
void I2C1sendbyte(char data);
char I2C1getbyte(void);

void initI2C()
{
    I2C1BRG = 607;      //this is the baud rate of a 100KHz clock
    I2C1CONbits.A10M = 0;   //7-bit address mode
    I2C1CONbits.I2CEN = 1;  //Enable the i2c module
}

void writeI2C(char _slaveAddress, char _regAddress,char data)
{
    I2C1Start();
    I2C1sendbyte(_slaveAddress);
    I2C1sendbyte(_regAddress);
    I2C1sendbyte(data);
    I2C1Stop();    
}
char readI2C(char _slaveAddress, char _regAddress, char readCount)
{
    I2C1Start();
    I2C1sendbyte(_slaveAddress);
    I2C1sendbyte(_regAddress);
    I2C1Start();
    I2C1sendbyte(readCount);
    char data = I2C1getbyte();
    I2C1Stop();    
}
void I2C1Start()
{
    I2C1CONbits.SEN = 1;
    while(I2C1CONbits.SEN); 
}
void I2C1sendbyte(char data)
{
    while(I2C1STATbits.TBF);
    I2C1TRN = data;
    while(I2C1STATbits.TBF);
    __delay_us(5);
}
char I2C1getbyte(void)
{
    I2C1CONbits.RCEN = 1;
    while(!I2C1STATbits.RBF);
    I2C1CONbits.ACKEN = 1;
    __delay_us(10);
    return(I2C1RCV);
}
void I2C1Stop()
{
    while(I2C1STATbits.TBF);
    I2C1CONbits.PEN = 1;
    while(I2C1CONbits.PEN);
}
