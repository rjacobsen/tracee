/* 
 * File:   DAC.h
 * Author: John
 *
 * Created on November 8, 2017, 4:26 PM
 */

#ifndef DAC_H
#define	DAC_H

#include "I2C.h"

void writeRGB(char r_byte, char g_byte, char b_byte);
void startSequence();

#endif	/* DAC_H */

