/* 
 * File:   LCD.h
 * Author: John
 *
 * Created on October 24, 2017, 6:32 PM
 */

#ifndef LCD_H
#define	LCD_H
#include "Definitions.h"

void initLCD();
void writeMsg(int line,char *msg);
void demoLCD();
void writeByte(char _dataByte,int rs);
#define ClrLCD()    writeByte(1,0)


#endif	/* LCD_H */

