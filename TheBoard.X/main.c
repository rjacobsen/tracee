
#include <stdbool.h>
#include <xc.h>
#include <stdlib.h>
#include <stdio.h>
#include "FastTransfer.h"
#include "Definitions.h"
#define FCY  60000000UL
#include <libpic30.h>
#include "Init.h"
#include "I2C.h"
#include "LCD.h"
#include "DAC.h"
// FICD
#pragma config ICS = PGD1               // ICD Communication Channel Select bits (Communicate on PGEC1 and PGED1)
#pragma config JTAGEN = OFF             // JTAG Enable bit (JTAG is disabled)

// FPOR
#pragma config ALTI2C1 = OFF            // Alternate I2C1 pins (I2C1 mapped to SDA1/SCL1 pins)
#pragma config ALTI2C2 = OFF            // Alternate I2C2 pins (I2C2 mapped to SDA2/SCL2 pins)
#pragma config WDTWIN = WIN25           // Watchdog Window Select bits (WDT Window is 25% of WDT period)

// FWDT
#pragma config WDTPOST = PS32768        // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128           // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = OFF             // PLL Lock Enable bit (Clock switch will not wait for the PLL lock signal.)
#pragma config WINDIS = OFF             // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable bit (Watchdog timer enabled/disabled by user software)

// FOSC
#pragma config POSCMD = HS              // Primary Oscillator Mode Select bits (HS Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF           // OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF            // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD           // Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FOSCSEL
#pragma config FNOSC = FRC              // Oscillator Source Selection (Internal Fast RC (FRC))
#pragma config IESO = OFF               // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FGS
#pragma config GWRP = OFF               // General Segment Write-Protect bit (General Segment may be written)
#pragma config GCP = OFF                // General Segment Code-Protect bit (General Segment Code protect is Disabled)

char msgArray[16];
int main()
{
    Initialize();
    demoLCD();
    //initI2C();
    //startSequence();
    
    while(1)
    {
        
        ToSend(LIDAR_COMMAND_INDEX, LIDAR_GET_HEADING,&OutGoing_DataTransBuff);
        sendData(BeaconAddress, &OutGoing_DataTransBuff);
        __delay_ms(50);
        ClrLCD();
        __delay_ms(5);
        LED5 ^= 1;
        receiveData();
        
        int data = receiveArrayAddress[1];
        sprintf(msgArray, "Ang1: %d", data);
        writeMsg(1,msgArray);
        sprintf(msgArray, "Ang2: %d", receiveArrayAddress[2] );
        writeMsg(2,msgArray);
                
    }
    return 0;
    
}
//#ifdef GENERAL_PIN8_INTERRUPT
void __attribute__ ((interrupt, no_auto_psv)) _CNInterrupt(void)
{   
    //clear the flag
    if(PORTAbits.RA8 == 0)
    {
        // PUT CODE HERE!!!!
    }
    IFS1bits.CNIF = 0;
}
//#endif