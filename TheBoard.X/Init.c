#include <stdbool.h>
#include <p33EP512GP504.h>

#include <xc.h>
#include <stdbool.h>
#include "FastTransfer.h"
#include "Init.h"
#include "Definitions.h"

#include "UART_handler.h"
#include <libpic30.h>

void initLEDs();
void toggleLED(int ledNUM, int state);
void LEDstartup();
void oscillator();
void INIT_GeneralPinInterrup();

int receiveArray[20];
void Initialize()
{
    //turn the global interrupts off
    GLOBAL_INTERRUPTS = 0;
    oscillator();
    initLEDs();
    UART_init();
    
    #ifdef GENERAL_PIN8_INTERRUPT
        // Even Though led8 is on the same pin we need to make this pin an input
        pinMODE_LED8 = INPUT;
        INIT_GeneralPinInterrup();
        
    #endif

    //initialize fast Transfer
    begin(receiveArray, sizeof(receiveArray),FastTransferAddress,false, Send_put,Receive_get,Receive_available,Receive_peek);
    //turn the Global interrupts on
    GLOBAL_INTERRUPTS = 1;
}
void initLEDs()
{
    //Initializing led pins to outputs
    pinMODE_LED1 = OUTPUT;
    pinMODE_LED2 = OUTPUT;
    pinMODE_LED3 = OUTPUT;
    pinMODE_LED4 = OUTPUT;
    pinMODE_LED5 = OUTPUT;
    pinMODE_LED6 = OUTPUT;
    pinMODE_LED7 = OUTPUT;
    pinMODE_LED8 = OUTPUT;

    int i;
    for(i = 1; i < 9;i++)
    {
        toggleLED(i,OFF);
    }
    LEDstartup();
}
void LEDstartup()
{
    int delayTime = 40;
    int i;
    for(i = 1; i < 9;i++)
    {
        toggleLED(i,ON);
        __delay_ms(delayTime);
    }
    for(i = 1; i < 9;i++)
    {
        toggleLED(i,OFF);
        __delay_ms(delayTime);
    }
    for(i = 9; i > 0;i--)
    {
        toggleLED(i,ON);
        __delay_ms(delayTime);
    }
    for(i = 9; i > 0;i--)
    {
        toggleLED(i,OFF);
        __delay_ms(delayTime);
    }
}
void toggleLED(int ledNUM, int state)
{
    switch(ledNUM)
    {
        case 1:
            LED1 = state;
            break;
        case 2:
            LED2 = state;
            break;
        case 3:
            LED3 = state;
            break;
        case 4:
            LED4 = state;
            break;
        case 5:
            LED5 = state;
            break;
        case 6:
            LED6 = state;
            break;
        case 7:
            LED7 = state;
            break;
        case 8:
            LED8 = state;
            break;
    }
}
void oscillator(void)
{
    // 120MHz 60MIPS
    // Configure PLL prescaler, PLL postscaler, PLL divisor
    PLLFBD = 58; // M=60
    CLKDIVbits.PLLPOST = 0; // N2=2
    CLKDIVbits.PLLPRE = 4; // N1=6

    // Initiate Clock Switch to PRI oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(OSCCON | 0x01);

    // Wait for Clock switch to occur
    while (OSCCONbits.COSC != 0b011);
    //    // Wait for PLL to lock
    while (OSCCONbits.LOCK != 1);
}

void INIT_GeneralPinInterrup()
{    
    #ifdef GENERAL_PIN8_INTERRUPT
        CNPUAbits.CNPUA8 = 1;    //enabling internal pull up
        CNPDAbits.CNPDA8 = 0;    //disabling internal pull down
        CNENAbits.CNIEA8 = 1;    //Enable RA8 pin for interrupt detection
        IEC1bits.CNIE = 1;       // Enables the Change Notification (CN) interrupts
        IFS1bits.CNIF = 0;       //Reset CN interrupt
    #endif
}