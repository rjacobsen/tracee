#include "DAC.h"
#include "Definitions.h"

#define BitMask_12bit   0xFFF
#define Upper8Mask      0xFF00
#define Lower8Mask      0xFF
#define scaleFactor     16      //16 = 0xFFF/0xFF (16 = 4096/255)
void writeDAC(int r_byte, int g_byte, int b_byte);
void writeDAC(int r_byte, int g_byte, int b_byte)
{
    r_byte = r_byte & BitMask_12bit;
    g_byte = g_byte & BitMask_12bit;
    b_byte = b_byte & BitMask_12bit;
    I2C1Start();
    I2C1sendbyte(0b11000000);
    I2C1sendbyte((g_byte & Upper8Mask) >> 8);      
    I2C1sendbyte(g_byte & Lower8Mask);
    I2C1sendbyte((b_byte & Upper8Mask) >> 8);        
    I2C1sendbyte(b_byte & Lower8Mask);
    I2C1sendbyte((r_byte & Upper8Mask) >> 8);       
    I2C1sendbyte(r_byte & Lower8Mask);
    I2C1Stop();
}
void writeRGB(char r_byte, char g_byte, char b_byte)
{
    int rVal = 0xFFF - (r_byte*scaleFactor);
    int gVal = 0xFFF - (g_byte*scaleFactor);
    int bVal = 0xFFF - (b_byte*scaleFactor);
    writeDAC(rVal,gVal,bVal);
}

void startSequence()
{ 
   unsigned int rgbColour[3];
   // Start off with red.
   rgbColour[0] = 0 ;
   rgbColour[1] = 0;
   rgbColour[2] = 0;  
   while(rgbColour[0] < 255)
   {
        writeRGB(rgbColour[0], rgbColour[1], rgbColour[2]);
        rgbColour[0]++;
        __delay_ms(5);
   }
   int decColour;
   // Choose the colors to increment and decrement.
   for (decColour = 0; decColour < 3; decColour += 1) {
     int incColour = decColour == 2 ? 0 : decColour + 1;

     // cross-fade the two colors.
     int i;
     for(i = 0; i < 255; i += 1) {
       rgbColour[decColour] -= 1;
       rgbColour[incColour] += 1;

       writeRGB(rgbColour[0], rgbColour[1], rgbColour[2]);
       __delay_ms(5);
     }
   }
   while(rgbColour[0] > 0 )
   {
        writeRGB(rgbColour[0], rgbColour[1], rgbColour[2]);
        rgbColour[0]--;
        __delay_ms(5);
   }
    
}