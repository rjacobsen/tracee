
#include <xc.h>
#include <stdbool.h>
#include <stdlib.h>
#include "UART_handler.h"
#include "Definitions.h"

#define ON         0
#define OFF        1
#define INDICATOR1 LATEbits.LATE5
#define INDICATOR2 LATEbits.LATE6
#define INDICATOR3 LATGbits.LATG7
#define INDICATOR4 LATGbits.LATG8
#define WATCHDOG   LATEbits.LATE7


//void *memset(void *s, int c, int n);
struct UART_ring_buff input_buffer;
struct UART_ring_buff output_buffer;


bool Transmit_stall = true;
bool Transmit_stall_gyro = true;

void UART_init(void)
{
   
    //****************************UART*************************
    RPINR18bits.U1RXR = 0b0101001; // RP41 for RX
    RPOR6bits.RP57R = 1; //configures the output TX to configurable pin RP57
    
    
    U1MODEbits.STSEL = 0; // 1-stop bit
    U1MODEbits.PDSEL = 0; // No parity, 8-data bits
    U1MODEbits.ABAUD = 0; // Auto-baud disabled
    U1BRG = BAUD_RATE; // Baud Rate setting for 115200
    U1STAbits.URXISEL = 0b11; // Interrupt after all RX character received
    U1STAbits.UTXISEL1 = 0; // Interrupt after all TX character Transmitted
    U1STAbits.UTXISEL0 = 1; // Interrupt after all TX character Transmitted
    IFS0bits.U1TXIF = 0; // Clear TX interrupt flag
    IFS0bits.U1RXIF = 0; // Clear RX interrupt flag
    IEC0bits.U1RXIE = 1;    //enables the RX interrupt
    IEC0bits.U1TXIE = 1;    //enables the TX interrupt
    U1MODEbits.UARTEN = 1; // Enable UART
    U1STAbits.UTXEN = 1; // Enable UART TX
    
    
    //TODO: enable the rx interrupt

    //UART_buff_init(&input_buffer);
    //UART_buff_init(&output_buffer);
    
}

//void UART_buff_init(struct UART_ring_buff* _this)
//{
//    /*****
//      The following clears:
//        -> buf
//        -> head
//        -> tail
//        -> count
//      and sets head = tail
//     ***/
//    memset(_this, 0, sizeof (*_this));
//}

void UART_buff_put(struct UART_ring_buff* _this, const unsigned char c)
{
    if (_this->count < UART_BUFFER_SIZE)
    {
        _this->buf[_this->head] = c;
        _this->head = UART_buff_modulo_inc(_this->head, UART_BUFFER_SIZE);
        ++_this->count;
    } else
    {
        _this->buf[_this->head] = c;
        _this->head = UART_buff_modulo_inc(_this->head, UART_BUFFER_SIZE);
        _this->tail = UART_buff_modulo_inc(_this->tail, UART_BUFFER_SIZE);

    }
}

unsigned char UART_buff_get(struct UART_ring_buff* _this)
{
    unsigned char c;
    if (_this->count > 0)
    {
        c = _this->buf[_this->tail];
        _this->tail = UART_buff_modulo_inc(_this->tail, UART_BUFFER_SIZE);
        --_this->count;
    } else
    {
        c = 0;
    }
    return (c);
}

void UART_buff_flush(struct UART_ring_buff* _this, const int clearBuffer)
{
    _this->count = 0;
    _this->head = 0;
    _this->tail = 0;
//    if (clearBuffer)
//    {
//        memset(_this->buf, 0, sizeof (_this->buf));
//    }
}

int UART_buff_size(struct UART_ring_buff* _this)
{
    return (_this->count);
}

unsigned int UART_buff_modulo_inc(const unsigned int value, const unsigned int modulus)
{
    unsigned int my_value = value + 1;
    if (my_value >= modulus)
    {
        my_value = 0;
    }
    return (my_value);
}

unsigned char UART_buff_peek(struct UART_ring_buff* _this)
{
    return _this->buf[_this->tail];
}

unsigned char Receive_peek(void)
{
    return UART_buff_peek(&input_buffer);
}

int Receive_available(void)
{
    return UART_buff_size(&input_buffer);
}

unsigned char Receive_get(void)
{
    return UART_buff_get(&input_buffer);
}

void Send_put(unsigned char _data)
{
    //LED1 ^=1;
    UART_buff_put(&output_buffer, _data);
    if(Transmit_stall == true)
    {
        Transmit_stall = false;
        U1TXREG = UART_buff_get(&output_buffer);
    }
}
void Send()
{
    if(Transmit_stall == true)
    {
        Transmit_stall = false;
        U1TXREG = UART_buff_get(&output_buffer);
    }
}

void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void)
{
    
    LED7 ^= 1;
    unsigned char data = U1RXREG;
    UART_buff_put(&input_buffer, data);
    IFS0bits.U1RXIF = 0; // Clear RX interrupt flag
}

void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void)
{
   
    if(output_buffer.count > 0)
    {
        U1TXREG = output_buffer.buf[output_buffer.tail];
        output_buffer.tail++;
        if(output_buffer.tail >= UART_BUFFER_SIZE)output_buffer.tail = 0;
        output_buffer.count--;
    }

    else
    {
        Transmit_stall = true;
    }

    IFS0bits.U1TXIF = 0; // Clear TX interrupt flag
}
