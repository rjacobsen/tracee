/* 
 * File:   I2C.h
 * Author: John
 *
 * Created on November 4, 2017, 4:10 PM
 */

#ifndef I2C_H
#define	I2C_H

void initI2C();
void writeI2C(char _slaveAddress, char _regAddress,char data);
char readI2C(char _slaveAddress, char _regAddress, char readCount);
void writeDAC();
void I2C1Stop();
void I2C1Start();
void I2C1sendbyte(char data);
char I2C1getbyte(void);

#endif	/* I2C_H */

