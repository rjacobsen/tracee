/******************************************************************
*  	The PID library Header file
*				System allows for easy creation of a PID controlled
*				Updatable system
*		Brought to you by:
*              Zac Kilburn
******************************************************************/
#ifndef PID_h
#define PID_h
#include "Arduino.h"


class PID
{	
public:
	PID(float target, float kp, float ki, float kd,int number);
	//PID(const int& target, float kp,float ki, float kd, int number);
/******************************************************************
This is where you will declare all of the methods
that will be able to be called by other segments of code externally
******************************************************************/
 int updateOutput(float sample);
 void setPropotionality(float kp, float ki, float kd);
 void clearIntegral();
 void verboseCalc();
 void updateTarget(float target);
 float readError();
 float readDerivative();
 float readIntegral();
 float readOutput();
 int readNumber();
 void clearSystem();
float returnTarget(void);
private:
/******************************************************************
Declare all internal variables and methods that will only be called 
and used internally
******************************************************************/
float _kp, _ki, _kd;
float _dt;
float _output;
float _error,_prevError,_dError;
int _now, _past;
float _derivative, _integral;
float _target;
int _number;
int* _targRef;

};
#endif
