/******************************************************************
*  Timers library for use on STACEE and CHRISTEE
*	The basic necessities for time keeping		
*
*		Brought to you by:
*              Zac Kilburn
******************************************************************/
#ifndef Timers_h
#define Timers_h
#include "Arduino.h"


class Timers
{	
public:
	Timers();
	Timers(long l);
	void resetTimer();
	void setInterval(long l);
	void updateTimer();
	bool timerDone();

private:
	long currentTime;
	long startTime;
	long length;
};
#endif
