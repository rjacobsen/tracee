#include "Timer.h"
#include <xc.h>
#include <stdbool.h>
#include "LEDs.h"

volatile long long milliseconds=0;

long long millis(void)
{
    return milliseconds;
}
void initTimer(void)
{
    
    T2CONbits.TCKPS = 0b11; // 256 divider
    PR2 = 2350; // 0.01s timer
    IPC1bits.T2IP = 1; // interrupt priority level 1
    IFS0bits.T2IF = 0; // clear interrupt flag
    IEC0bits.T2IE = 1; // enable timer 2 interrupt
    T2CONbits.TON = 1; // turn on timer
    

    
}

bool timerDone(timer_t * t){
    if((millis()-t->lastTime)>t->interval)
    {
        t->lastTime=millis();
        return true;
    }
    else{
    return false; 
    }
}

void setTimerInterval(timer_t *t,long long i){
    t->interval=i;
}
void resetTimer(timer_t * t)
{
    
        t->lastTime=millis();
}
bool flipflop=true;


//10ms timer
void __attribute__((interrupt, no_auto_psv)) _T2Interrupt(void)
{
    milliseconds+=10;
//    if (flipflop)
//    {   
//    SetDBG1(1); 
//    }
//    else
//    {
//    SetDBG1(0);
//    
//    }
//    flipflop= !flipflop;

    IFS0bits.T2IF = 0; // clear interrupt flag
}

