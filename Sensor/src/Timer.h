/* 
 * File:   Timer.h
 * Author: Zac
 *
 * Created on May 20, 2017, 3:44 PM
 */

#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif
#include <stdbool.h>
    
    typedef struct{
        long long lastTime;
        long long interval;
    }timer_t;

void initTimer(void);
bool timerDone(timer_t * t);
void setTimerInterval(timer_t *t, long long i);

void resetTimer(timer_t * t);
long long millis(void);


#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

