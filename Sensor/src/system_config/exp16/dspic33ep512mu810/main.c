
// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include <xc.h>
#include "ConfigBits.h"
#include "Initialize.h"
#include "CANFastTransfer.h"
#include "Communications.h"
#include "common.h"
#include "LEDs.h"
#include "Timer.h"
#include "definitions.h"
#include <stdio.h>
#include "..\..\..\tracee\Common\CommsDefenition.h"
#define FCY 60000000UL
#include <libpic30.h>

long long lastMillis=0;
int main(void)
{
    InitDBG(); 
    SetDBG1(1);
    SetDBG2(1); 
    SetDBG3(1); 
    SetDBG4(1); 
    initialize();
    initTimer();
    SetDBG3(0); 
    SetDBG4(0); 
	//can_init(ReceiveCANFast);
    int j = 0; 
    /* Loop infinitely */
    while( 1 ){
//            __delay_ms(1000);
//            ToSendCAN(1,1);
//            ToSendCAN(2,2);
//            sendDataCAN(MouseGyroAddress);
        if((millis()-lastMillis)>1000)
        {
            if(j == 0) 
            {
                SetDBG3(0);
                j = 1; 
            }    
            else 
            {
                SetDBG3(1);
                j = 0; 
            }
            lastMillis=millis();
        }
            
        if(ReceiveDataCAN()) 
        {           
            printf("Can Receive\r\n");
            updateCommunications();
        }
       
        
//        if(ReceiveArray[4] == 4) {
//            SetDBG3(1); 
//        }
//        //can_tx(&test);
//        ToSendCAN(6,6);
//        ToSendCAN(5, 5); 
//        ToSendCAN(4, 4);
//        ToSendCAN(3,3);
//        ToSendCAN(2, 2);
//        ToSendCAN(1, 1);
//        sendDataCAN(RouterCardAddress);
//        
//        //ToSendCAN(6,12);
//        //ToSendCAN(5, 10); 
//        ToSendCAN(4, 8);
//        ToSendCAN(3,6);
//        ToSendCAN(2, 4);
//        ToSendCAN(1, 2);
//        sendDataCAN(ControlBoxAddress);
//        __delay32(120000); //1ms delay
//         ToSendCAN(9, 18);
//        ToSendCAN(8,16);
//        ToSendCAN(7, 14);
//        ToSendCAN(6, 12);
//        sendDataCAN(ControlBoxAddress);
//        __delay32(6000000); //100ms delay
        
    }
}
