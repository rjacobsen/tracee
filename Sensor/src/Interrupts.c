#include "Interrupts.h"
#include <xc.h>

/******************************************************************************
 * Function:      void __attribute__((interrupt, no_auto_psv))_C2Interrupt(void)
 *
 * PreCondition:  None
 *
 * Input:         None
 *
 * Output:        None
 *
 * Side Effects:  None
 *
 * Overview:      Interrupt service routine to handle CAN2 Transmit and
 *                recieve interrupt.
 *****************************************************************************/
void __attribute__ ( (interrupt, no_auto_psv) ) _C2Interrupt( void )
{
    IFS3bits.C2IF = 0;      // clear interrupt flag
    if( C2INTFbits.TBIF )
    {
        C2INTFbits.TBIF = 0;
    }

    if( C2INTFbits.RBIF )
    {
        C2INTFbits.RBIF = 0;
    }
}

//------------------------------------------------------------------------------
//    DMA interrupt handlers
//------------------------------------------------------------------------------
/******************************************************************************
 * Function:   void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void)
 *
 * PreCondition:  None
 *
 * Input:         None
 *
 * Output:        None
 *
 * Side Effects:  None
 *
 * Overview:      Interrupt service routine to handle DMA0interrupt
 *****************************************************************************/
void __attribute__ ( (interrupt, no_auto_psv) ) _DMA0Interrupt( void )
{
    IFS0bits.DMA0IF = 0;    // Clear the DMA0 Interrupt Flag;
}

/******************************************************************************
 * Function:   void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void)
 *
 * PreCondition:  None
 *
 * Input:         None
 *
 * Output:        None
 *
 * Side Effects:  None
 *
 * Overview:      Interrupt service routine to handle DMA1interrupt
 *****************************************************************************/
void __attribute__ ( (interrupt, no_auto_psv) ) _DMA1Interrupt( void )
{
    IFS0bits.DMA1IF = 0;    // Clear the DMA1 Interrupt Flag;
}

/******************************************************************************
 * Function:   void __attribute__((interrupt, no_auto_psv)) _DMA2Interrupt(void)
 *
 * PreCondition:  None
 *
 * Input:         None
 *
 * Output:        None
 *
 * Side Effects:  None
 *
 * Overview:      Interrupt service routine to handle DMA2interrupt
 *****************************************************************************/
void __attribute__ ( (interrupt, no_auto_psv) ) _DMA2Interrupt( void )
{
    //SetDBG3(1); 
    IFS1bits.DMA2IF = 0;    // Clear the DMA2 Interrupt Flag;
}

/******************************************************************************
 * Function:   void __attribute__((interrupt, no_auto_psv)) _DMA3Interrupt(void)
 *
 * PreCondition:  None
 *
 * Input:         None
 *
 * Output:        None
 *
 * Side Effects:  None
 *
 * Overview:      Interrupt service routine to handle DMA3interrupt
 *****************************************************************************/
void __attribute__ ( (interrupt, no_auto_psv) ) _DMA3Interrupt( void )
{
    IFS2bits.DMA3IF = 0;    // Clear the DMA3 Interrupt Flag;
}