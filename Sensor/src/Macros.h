/* 
 * File:   Macros.h
 * Author: Zac
 *
 * Created on May 12, 2017, 3:45 PM
 */

#ifndef MACROS_H
#define	MACROS_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef enum{
    noMacro=0,
    turnMacro=4,
    driveMacro=6,
    fullAutonomy
}macro_select_t;


void initMacro(macro_select_t macro);
void squareRoutine(void);
void fullyAutonomyRoutine(void);


#ifdef	__cplusplus
}
#endif

#endif	/* MACROS_H */

