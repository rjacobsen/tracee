#include "Initialize.h"
#include <xc.h>
#include "LEDs.h"
#include "CANFastTransfer.h"
#include "UART.h"
#include <stdio.h>


void initialize(void)
{
    
    INTCON2bits.GIE = 0;
    /* Configure Oscillator Clock Source     */
    OscConfig();
    SetDBG1(0); 
    /* Clear Interrupt Flags                 */
    ClearIntrflags();
    SetDBG2(0); 
    initCANFT();
    //InitDBG();
    initUART();
    INTCON2bits.GIE = 1; 
    printf("Bootup Complete\r\n");
}
/******************************************************************************
 * Function:        void ClearIntrflags(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:         Clears all the interrupt flag registers.
 *****************************************************************************/
void ClearIntrflags( void )
{
    /* Clear Interrupt Flags */
    IFS0 = 0;
    IFS1 = 0;
    IFS2 = 0;
    IFS3 = 0;
    IFS4 = 0;
}

/******************************************************************************
 * Function:        void OscConfig(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:       This function configures the Oscillator to work at 60MHz.
 *****************************************************************************/
void OscConfig( void )
{
    // 120MHz 60MIPS
    // Configure PLL prescaler, PLL postscaler, PLL divisor
    PLLFBD = 58; // M=60
    CLKDIVbits.PLLPOST = 0; // N2=2
    CLKDIVbits.PLLPRE = 4; // N1=6

    // Initiate Clock Switch to PRI oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(OSCCON | 0x01);

    // Wait for Clock switch to occur
    while (OSCCONbits.COSC != 0b011);
    //    // Wait for PLL to lock
    while (OSCCONbits.LOCK != 1);
}




/*******************************************************************************
 End of File
*/
