/* 
 * File:   Initialize.h
 * Author: Zac
 *
 * Created on May 6, 2017, 6:27 AM
 */

#ifndef INITIALIZE_H
#define	INITIALIZE_H

#ifdef	__cplusplus
extern "C" {
#endif

void                            initialize( void );
void                            OscConfig( void );
void                            ClearIntrflags( void );
void                            Ecan1WriteMessage( void );


#ifdef	__cplusplus
}
#endif

#endif	/* INITIALIZE_H */

