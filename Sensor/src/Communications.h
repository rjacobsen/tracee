/* 
 * File:   Communications.h
 * Author: Zac
 *
 * Created on May 12, 2017, 3:38 PM
 */

#ifndef COMMUNICATIONS_H
#define	COMMUNICATIONS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdbool.h>
#include "Macros.h"
 

void updateCommunications(void);
void checkMacroCommand(void);
macro_select_t returnMacroCommand(void);
bool returnGyroMacroActive(void);
void stopMacroCommandGyro(void);
void setMacroActiveGyro(bool b);
void setMacroCommand(macro_select_t b);



#ifdef	__cplusplus
}
#endif

#endif	/* COMMUNICATIONS_H */

