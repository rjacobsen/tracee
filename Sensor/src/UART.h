/* 
 * File:   UART.h
 * Author: Zac
 *
 * Created on August 4, 2017, 2:27 PM
 */

#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>

void initUART(void);
void UART_Write(uint8_t c);
uint8_t UART_Read( void);


#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

