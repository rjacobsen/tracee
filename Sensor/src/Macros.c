#include <xc.h>
#include "Macros.h"
#include "Communications.h"
#include "../../Common/CommsDefenition.h"
#include "CANFastTransfer.h"
#include "LEDs.h"
#include "Timer.h"
#define DRIVE_MACRO_GYRO             6
#define TURN_MACRO_GYRO              4
#define SQUARE_ROUTINE_STRAIGHT_DIST 50
#define SQUARE_ROUTINE_TURN_90       90

void initMacro(macro_select_t macro)
{
    switch(macro)
    {
        case noMacro:
            
            break;
        case turnMacro:
            
            break;
        case driveMacro:
            
            break;
        case fullAutonomy:
            
            break;
    }
}


void fullyAutonomyRoutine(void)
{
    stopMacroCommandGyro();
}

timer_t macroDisplayTimer;
timer_t macroContinueTimer;

typedef enum{
    driveStraight,
    turn90,
    exit
}squareRoutine_t;

squareRoutine_t s=driveStraight;

void squareRoutine(void)
{
    setTimerInterval(&macroDisplayTimer,100);
    setTimerInterval(&macroContinueTimer,100);
    s=driveStraight;
    //Keep going until the control box says that we are done
    while(returnMacroCommand()!=noMacro)
    {
        switch(s)
        {
            case driveStraight:
                
                if(timerDone(&macroContinueTimer))
                {
                    ToSendCAN(1,DRIVE_MACRO_GYRO);
                    ToSendCAN(2,SQUARE_ROUTINE_STRAIGHT_DIST);
                    sendDataCAN(MouseGyroAddress);
                    setMacroActiveGyro(true);
                    //While the control box says keep going and the gyro says active
                    while(returnMacroCommand()!=noMacro && returnGyroMacroActive())
                    {
                        if(timerDone(&macroDisplayTimer))
                        {
                            ToggleDBG1();
                        }
                        if(ReceiveDataCAN())
                            updateCommunications();
                    }
                    s=turn90;
                    resetTimer(&macroContinueTimer);
                }
                break;
                
            case turn90:
                if(timerDone(&macroContinueTimer))
                {
                    ToSendCAN(1,TURN_MACRO_GYRO);
                    ToSendCAN(2,SQUARE_ROUTINE_TURN_90);
                    sendDataCAN(MouseGyroAddress);
                    setMacroActiveGyro(true);
                    while(returnMacroCommand()!=noMacro && returnGyroMacroActive())
                    {
                        if(timerDone(&macroDisplayTimer))
                        {
                            ToggleDBG1();
                        }
                        if(ReceiveDataCAN())
                            updateCommunications();
                    }
                    s=driveStraight;
                    resetTimer(&macroContinueTimer);
                }
                break;
            case exit:
                setMacroCommand(noMacro);
                break;
        }   
    }
    stopMacroCommandGyro();
}