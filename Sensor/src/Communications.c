#include <xc.h>
#include "Communications.h"
#include "../../Common/CommsDefenition.h"
#include "CANFastTransfer.h"

macro_select_t macroCommand;
int macroSubCommand=0;
bool macroActiveGyro=false;

macro_select_t returnMacroCommand(void)
{
    return macroCommand;
}
void setMacroCommand(macro_select_t b)
{
    macroCommand=b;
}

bool returnGyroMacroActive(void)
{
    return macroActiveGyro;
}

void setMacroActiveGyro(bool b)
{
    macroActiveGyro=b;
}
void updateCommunications(void)
{
    ToggleDBG4();
    int * receivable= getReceiveArray();
    switch(receivable[LastBoardReceived])
    {
        case RouterCardAddress:  
            //Data from the router card
            macroCommand=receivable[1];
            macroSubCommand=receivable[2]; 
            //Always check the macro command
            checkMacroCommand();
            break;
        case MouseGyroAddress:
            //ToggleDBG4();
            //data from the mouse gyro board
            macroActiveGyro = receivable[7];
            break;
    }   
    //receivable[LastBoardReceived]=88;
}


void checkMacroCommand(void)
{
    switch(macroCommand)
    {
        case noMacro:
            //Do something here
            ToSendCAN(1,0);
            ToSendCAN(2,0);
            sendDataCAN(MouseGyroAddress);
            break;
        case turnMacro:
        case driveMacro:
            //Both of these need to be directly pushed to the mouseGyro board to be processed
            ToSendCAN(1,macroCommand);
            ToSendCAN(2,macroSubCommand);
            sendDataCAN(MouseGyroAddress);
            break;
        case fullAutonomy:
            macroActiveGyro=true;
            squareRoutine();
            break;
        
    }
}

void stopMacroCommandGyro(void)
{
    ToSendCAN(1,0);
    ToSendCAN(2,0);
    sendDataCAN(MouseGyroAddress);           
}