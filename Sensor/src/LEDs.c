/*
 * File:   test.c
 * Author: reed
 *
 * Created on August 13, 2016, 2:48 PM
 */


#include <xc.h>
#include "LEDs.h"

void InitDBG() {
        ANSELEbits.ANSE5 = 0; 
        TRISEbits.TRISE5 = 0; 
        ANSELEbits.ANSE6 = 0;
        TRISEbits.TRISE6 = 0; 
        TRISGbits.TRISG7 = 0; 
        TRISGbits.TRISG8 = 0; 
        
        PORTEbits.RE5 = 1; 
        PORTEbits.RE6 = 1; 
        PORTGbits.RG7 = 1; 
        PORTGbits.RG8 = 1; 
    }
void SetDBG1(int i) {
        if(i) {
           LATEbits.LATE5 = 0;
        }
        else {
            LATEbits.LATE5 = 1;
        }
    }
    
    void SetDBG2(int i) {
        if(i) {
            PORTEbits.RE6 = 0;
        }
        else {
            PORTEbits.RE6 = 1;
        }
    }
    
    void SetDBG3(int i) {
        if(i) {
            PORTGbits.RG7 = 0;
        }
        else {
            PORTGbits.RG7 = 1;
        }
    }
    
    void SetDBG4(int i) {
        if(i) {
            PORTGbits.RG8 = 0;
        }
        else {
            PORTGbits.RG8 = 1;
        }
    }
    
    void ToggleDBG1(void)
    {
        LATEbits.LATE5 ^=1;
    }

    void ToggleDBG2(void)
    {
        LATEbits.LATE6 ^=1;
    }

    void ToggleDBG3(void)
    {
        LATGbits.LATG7 ^=1;
    }

    void ToggleDBG4(void)
    {
        LATGbits.LATG8 ^=1;
    }
