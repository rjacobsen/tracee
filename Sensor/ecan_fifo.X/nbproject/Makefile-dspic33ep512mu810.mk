#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-dspic33ep512mu810.mk)" "nbproject/Makefile-local-dspic33ep512mu810.mk"
include nbproject/Makefile-local-dspic33ep512mu810.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=dspic33ep512mu810
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/ecan1drv.c ../src/traps.c ../src/system_config/exp16/dspic33ep512mu810/ecan1_config.c ../src/system_config/exp16/dspic33ep512mu810/main.c ../src/can.c ../src/CANFastTransfer.c "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Initialize.c" ../src/LEDs.c "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Interrupts.c" "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Communications.c" ../src/Macros.c ../src/UART.c ../src/Timer.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1360937237/ecan1drv.o ${OBJECTDIR}/_ext/1360937237/traps.o ${OBJECTDIR}/_ext/60018555/ecan1_config.o ${OBJECTDIR}/_ext/60018555/main.o ${OBJECTDIR}/_ext/1360937237/can.o ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o ${OBJECTDIR}/_ext/446738168/Initialize.o ${OBJECTDIR}/_ext/1360937237/LEDs.o ${OBJECTDIR}/_ext/446738168/Interrupts.o ${OBJECTDIR}/_ext/446738168/Communications.o ${OBJECTDIR}/_ext/1360937237/Macros.o ${OBJECTDIR}/_ext/1360937237/UART.o ${OBJECTDIR}/_ext/1360937237/Timer.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1360937237/ecan1drv.o.d ${OBJECTDIR}/_ext/1360937237/traps.o.d ${OBJECTDIR}/_ext/60018555/ecan1_config.o.d ${OBJECTDIR}/_ext/60018555/main.o.d ${OBJECTDIR}/_ext/1360937237/can.o.d ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o.d ${OBJECTDIR}/_ext/446738168/Initialize.o.d ${OBJECTDIR}/_ext/1360937237/LEDs.o.d ${OBJECTDIR}/_ext/446738168/Interrupts.o.d ${OBJECTDIR}/_ext/446738168/Communications.o.d ${OBJECTDIR}/_ext/1360937237/Macros.o.d ${OBJECTDIR}/_ext/1360937237/UART.o.d ${OBJECTDIR}/_ext/1360937237/Timer.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1360937237/ecan1drv.o ${OBJECTDIR}/_ext/1360937237/traps.o ${OBJECTDIR}/_ext/60018555/ecan1_config.o ${OBJECTDIR}/_ext/60018555/main.o ${OBJECTDIR}/_ext/1360937237/can.o ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o ${OBJECTDIR}/_ext/446738168/Initialize.o ${OBJECTDIR}/_ext/1360937237/LEDs.o ${OBJECTDIR}/_ext/446738168/Interrupts.o ${OBJECTDIR}/_ext/446738168/Communications.o ${OBJECTDIR}/_ext/1360937237/Macros.o ${OBJECTDIR}/_ext/1360937237/UART.o ${OBJECTDIR}/_ext/1360937237/Timer.o

# Source Files
SOURCEFILES=../src/ecan1drv.c ../src/traps.c ../src/system_config/exp16/dspic33ep512mu810/ecan1_config.c ../src/system_config/exp16/dspic33ep512mu810/main.c ../src/can.c ../src/CANFastTransfer.c C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Initialize.c ../src/LEDs.c C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Interrupts.c C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Communications.c ../src/Macros.c ../src/UART.c ../src/Timer.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-dspic33ep512mu810.mk dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33EP512MC806
MP_LINKER_FILE_OPTION=,--script=p33EP512MC806.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1360937237/ecan1drv.o: ../src/ecan1drv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ecan1drv.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ecan1drv.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/ecan1drv.c  -o ${OBJECTDIR}/_ext/1360937237/ecan1drv.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/ecan1drv.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ecan1drv.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/traps.o: ../src/traps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/traps.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/traps.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/traps.c  -o ${OBJECTDIR}/_ext/1360937237/traps.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/traps.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/traps.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/60018555/ecan1_config.o: ../src/system_config/exp16/dspic33ep512mu810/ecan1_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60018555" 
	@${RM} ${OBJECTDIR}/_ext/60018555/ecan1_config.o.d 
	@${RM} ${OBJECTDIR}/_ext/60018555/ecan1_config.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/exp16/dspic33ep512mu810/ecan1_config.c  -o ${OBJECTDIR}/_ext/60018555/ecan1_config.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/60018555/ecan1_config.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/60018555/ecan1_config.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/60018555/main.o: ../src/system_config/exp16/dspic33ep512mu810/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60018555" 
	@${RM} ${OBJECTDIR}/_ext/60018555/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/60018555/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/exp16/dspic33ep512mu810/main.c  -o ${OBJECTDIR}/_ext/60018555/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/60018555/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/60018555/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/can.o: ../src/can.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/can.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/can.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/can.c  -o ${OBJECTDIR}/_ext/1360937237/can.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/can.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/can.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o: ../src/CANFastTransfer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/CANFastTransfer.c  -o ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/446738168/Initialize.o: C:/Users/Zac/Desktop/TRACEE\ REPO/tracee/Sensor/src/Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/446738168" 
	@${RM} ${OBJECTDIR}/_ext/446738168/Initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/446738168/Initialize.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Initialize.c"  -o ${OBJECTDIR}/_ext/446738168/Initialize.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/446738168/Initialize.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/446738168/Initialize.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/LEDs.o: ../src/LEDs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LEDs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LEDs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/LEDs.c  -o ${OBJECTDIR}/_ext/1360937237/LEDs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/LEDs.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/LEDs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/446738168/Interrupts.o: C:/Users/Zac/Desktop/TRACEE\ REPO/tracee/Sensor/src/Interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/446738168" 
	@${RM} ${OBJECTDIR}/_ext/446738168/Interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/446738168/Interrupts.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Interrupts.c"  -o ${OBJECTDIR}/_ext/446738168/Interrupts.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/446738168/Interrupts.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/446738168/Interrupts.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/446738168/Communications.o: C:/Users/Zac/Desktop/TRACEE\ REPO/tracee/Sensor/src/Communications.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/446738168" 
	@${RM} ${OBJECTDIR}/_ext/446738168/Communications.o.d 
	@${RM} ${OBJECTDIR}/_ext/446738168/Communications.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Communications.c"  -o ${OBJECTDIR}/_ext/446738168/Communications.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/446738168/Communications.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/446738168/Communications.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/Macros.o: ../src/Macros.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Macros.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Macros.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/Macros.c  -o ${OBJECTDIR}/_ext/1360937237/Macros.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/Macros.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Macros.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/UART.o: ../src/UART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/UART.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/UART.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/UART.c  -o ${OBJECTDIR}/_ext/1360937237/UART.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/UART.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/UART.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/Timer.o: ../src/Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Timer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/Timer.c  -o ${OBJECTDIR}/_ext/1360937237/Timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/Timer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/1360937237/ecan1drv.o: ../src/ecan1drv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ecan1drv.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ecan1drv.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/ecan1drv.c  -o ${OBJECTDIR}/_ext/1360937237/ecan1drv.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/ecan1drv.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ecan1drv.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/traps.o: ../src/traps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/traps.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/traps.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/traps.c  -o ${OBJECTDIR}/_ext/1360937237/traps.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/traps.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/traps.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/60018555/ecan1_config.o: ../src/system_config/exp16/dspic33ep512mu810/ecan1_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60018555" 
	@${RM} ${OBJECTDIR}/_ext/60018555/ecan1_config.o.d 
	@${RM} ${OBJECTDIR}/_ext/60018555/ecan1_config.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/exp16/dspic33ep512mu810/ecan1_config.c  -o ${OBJECTDIR}/_ext/60018555/ecan1_config.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/60018555/ecan1_config.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/60018555/ecan1_config.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/60018555/main.o: ../src/system_config/exp16/dspic33ep512mu810/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/60018555" 
	@${RM} ${OBJECTDIR}/_ext/60018555/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/60018555/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/exp16/dspic33ep512mu810/main.c  -o ${OBJECTDIR}/_ext/60018555/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/60018555/main.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/60018555/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/can.o: ../src/can.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/can.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/can.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/can.c  -o ${OBJECTDIR}/_ext/1360937237/can.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/can.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/can.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o: ../src/CANFastTransfer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/CANFastTransfer.c  -o ${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/CANFastTransfer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/446738168/Initialize.o: C:/Users/Zac/Desktop/TRACEE\ REPO/tracee/Sensor/src/Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/446738168" 
	@${RM} ${OBJECTDIR}/_ext/446738168/Initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/446738168/Initialize.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Initialize.c"  -o ${OBJECTDIR}/_ext/446738168/Initialize.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/446738168/Initialize.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/446738168/Initialize.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/LEDs.o: ../src/LEDs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LEDs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LEDs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/LEDs.c  -o ${OBJECTDIR}/_ext/1360937237/LEDs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/LEDs.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/LEDs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/446738168/Interrupts.o: C:/Users/Zac/Desktop/TRACEE\ REPO/tracee/Sensor/src/Interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/446738168" 
	@${RM} ${OBJECTDIR}/_ext/446738168/Interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/446738168/Interrupts.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Interrupts.c"  -o ${OBJECTDIR}/_ext/446738168/Interrupts.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/446738168/Interrupts.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/446738168/Interrupts.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/446738168/Communications.o: C:/Users/Zac/Desktop/TRACEE\ REPO/tracee/Sensor/src/Communications.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/446738168" 
	@${RM} ${OBJECTDIR}/_ext/446738168/Communications.o.d 
	@${RM} ${OBJECTDIR}/_ext/446738168/Communications.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  "C:/Users/Zac/Desktop/TRACEE REPO/tracee/Sensor/src/Communications.c"  -o ${OBJECTDIR}/_ext/446738168/Communications.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/446738168/Communications.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/446738168/Communications.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/Macros.o: ../src/Macros.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Macros.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Macros.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/Macros.c  -o ${OBJECTDIR}/_ext/1360937237/Macros.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/Macros.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Macros.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/UART.o: ../src/UART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/UART.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/UART.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/UART.c  -o ${OBJECTDIR}/_ext/1360937237/UART.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/UART.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/UART.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/Timer.o: ../src/Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Timer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/Timer.c  -o ${OBJECTDIR}/_ext/1360937237/Timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/Timer.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -I"../src" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x1000:0x101B -mreserve=data@0x101C:0x101D -mreserve=data@0x101E:0x101F -mreserve=data@0x1020:0x1021 -mreserve=data@0x1022:0x1023 -mreserve=data@0x1024:0x1027 -mreserve=data@0x1028:0x104F   -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_dspic33ep512mu810=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/ecan_fifo.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/dspic33ep512mu810
	${RM} -r dist/dspic33ep512mu810

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
