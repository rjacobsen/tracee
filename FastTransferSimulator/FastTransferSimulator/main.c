/*
 * FastTransferSimulator.cpp
 *
 * Created: 2/3/2018 7:36:47 PM
 * Author : reed
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

#include "..\..\..\tracee\Common\CommsDefenition.h"
#include "ATMEGA328P_UART.h"
#include "FastTransfer.h"

//Define Pins 
//output
#define led PORTB5

//input
#define In_1 PORTD7
#define In_2 PORTD6
#define Pot_In PORTC0 //also adc0

//Prototype Functions
void initTimers();
char findMode();
//global variables
volatile char resend = 0; 
char oldMode = -1; //start with invalid mode
volatile char Mode = 0; //findMode();

int main()
{
	DDRB = 1<<led; //set pin 5 to output
	PORTB = 1<<led;
	

	//Initialize Timers
	initTimers();
	
	initFastTransfer(ControlBoxAddress);
	sei();

	
	
	//Main Loop
    while (1) 
    {
		
		
		if(resend) {;
			resend = 0;
			//have four different modes
			Mode = findMode();
			if(Mode != oldMode){
				//re-initialize Fast Transfer to have new address
				oldMode = Mode;
				switch(Mode) {
					case 0: //0 - pretends to be control box sending to Mouse/Gyro
						initFastTransfer(ControlBoxAddress);
						break;
					case 1: //1 - pretends to be control box sending to Router Card
						initFastTransfer(ControlBoxAddress);
						break;
					case 2: //2 - pretends to be beacon sending to Mouse/Gyro
						initFastTransfer(BeaconAddress);
						break;
					case 3: //3 - pretends to be beacon sending to control Box
						initFastTransfer(BeaconAddress);
						break;
					//default:
					//do nothing
				}
			}
			else {
				switch(Mode) {
					case 0: //0 - pretends to be control box sending to Mouse/Gyro
						ToSend0(1, 1);
						ToSend0(2, 2);
						sendData0(MouseGyroAddress);
						break;
					case 1: //1 - pretends to be control box sending to Router Card
						ToSend0(1, 1);
						ToSend0(2, 2);
						sendData0(RouterCardAddress);
						break;
					case 2: //2 - pretends to be beacon sending to Mouse/Gyro
						ToSend0(1, 1);
						ToSend0(2, 2);
						sendData0(MouseGyroAddress);
						break;
					case 3: //3 - pretends to be beacon sending to control Box
						ToSend0(1, 1);
						ToSend0(2, 2);
						sendData0(ControlBoxAddress);
						break;
					//default:
						//do nothing
				}
			}
		}
//
	}
}

char findMode(){
	//char temp = (PIND & (1<<In_1))>>In_1 | (PIND & (1<<In_2))>>(In_2-1);
	char temp = 0;
	if(PIND & (1<<In_1) && PIND & (1<<In_2))
		temp = 3;
	else if(PIND & (1<<In_1))
		temp = 1;
	else if(PIND & (1<<In_2))
		temp = 2;	
	//OCR1A = 15625/temp;
	return temp;
}

void initTimers(void)
{
	/* Timer clock = I/O clock / 1024 */
	TCCR0B = (1<<CS02)|(1<<CS00);
	TCCR1B = (1<<CS12)|(1<<CS10);
	/* Clear overflow flag */
	TIFR0  = 1<<TOV0;
	TIFR1 = 1<<TOV0;
	/* Enable Overflow Interrupt */
	TIMSK1 = 1<< OCIE1A;


	//setup timer 1 OCA for 0.1 sec given 1024 division
	OCR1A = 1562;
	//setup timer 1 OCB for 1 sec given 1024 division
	//OCR1B = 15625;
	
	//reset TCNT of both timers
	TCNT0 = 0;
	TCNT1 = 0; 
}

//timer1 COMPA
ISR(TIMER1_COMPA_vect) {
	TCNT1 = 0;
	modeLEDBlink();
	resend = 1;
}

volatile int counter = 0; 
void modeLEDBlink() {
	
	if(counter < (Mode+1)*2) {
		PORTB ^=(1<<led);
		counter++;
		return;
	}
	if(counter < 10) {
		//PORTB ^=(1<<led);
		counter++;
		return;
	}
	counter = 0;
}