/*
 * ATMEGA328P_UART.c
 *
 * Created: 4/27/2016 9:57:15 PM
 *  Author: reed
 */ 

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include "ATMEGA328P_UART.h"
#define F_CPU 16000000


circular_buffer UART0_TX_Buffer;
#define UART0_TX_Buffer_Size 102
circular_buffer UART0_RX_Buffer;
#define UART0_RX_Buffer_Size 102
char received; 




void cb_init(circular_buffer *cb, int capacity, int sz)
{
	cb->buffer = malloc(capacity * sz);
	if(cb->buffer == 0)
	{
		//PORTA &= ~(1<<6); //turn on error 2
	}
	// handle error
	cb->buffer_end = (int *)cb->buffer + capacity * sz;
	cb->capacity = capacity;
	cb->count = 0;
	cb->sz = sz;
	cb->head = cb->buffer;
	cb->tail = cb->buffer;
}

void cb_free(circular_buffer *cb)
{
	free(cb->buffer);
	// clear out other fields too, just to be safe
}

void cb_push_back(circular_buffer *cb, void *item)
{
	if(cb->count == cb->capacity)
	{
		return; 
	}
	// handle error
	memcpy(cb->head, item, cb->sz);
	cb->head = (int*)cb->head + cb->sz;
	if(cb->head == cb->buffer_end)
	cb->head = cb->buffer;
	cb->count++;
}

void cb_pop_front(circular_buffer *cb, void *item)
{
	//int *item = NULL;
	if(cb->count == 0)
	{
		return; 
	}
	// handle error
	memcpy(item, cb->tail, cb->sz);
	cb->tail = (int*)cb->tail + cb->sz;
	if(cb->tail == cb->buffer_end)
	cb->tail = cb->buffer;
	cb->count--;
	//return *item;
}

void cb_peek_front(circular_buffer *cb, void *item) 
{
	//int *item = NULL;
	if(cb->count == 0);
	
	memcpy(item, cb->tail, cb->sz); 
	//return *item; 
}

int cb_size(circular_buffer *cb) {
	return cb->count;
}

void USART0_Init( unsigned long baud) {
	switch (baud)
	{
		case 115200:
			UBRR0 = 8;
			break;
		case 57600:
			UBRR0 = 16;
			break;
		case 38400:
			UBRR0 = 25;
			break;
		case 19200:
			UBRR0 = 51;
			break;
		default: //9600
			UBRR0 = 103; 
	}
	
	UCSR0C	= (0<<UMSEL00) | (0<<UMSEL01) | (0<<UPM01) | (0<<UPM00) | (1<<USBS0) | (1<<UCSZ00) | (1<<UCSZ01);
	UCSR0B = (1<<RXEN0) | (1<<TXEN0) | (1<<RXCIE0);
	
	cb_init(&UART0_TX_Buffer, UART0_TX_Buffer_Size, 1);
	cb_init(&UART0_RX_Buffer, UART0_RX_Buffer_Size, 1);
}


// void mon_putc (char ch)
// {
// 	USART0_put_C(ch);
// }

void USART0_put_C (unsigned char data)
{
	UCSR0B &=  ~(1<<UDRIE0); // must disable interrupt to ensure adding character isn't interrupted.
	cb_push_back(&UART0_TX_Buffer, &data);
	UCSR0B |= (1<<UDRIE0);
}



unsigned char USART0_get_C(){
	char val;
	cb_pop_front(&UART0_RX_Buffer, &val);
	return val;
}

unsigned char USART0_peek_C() {
	char val;
	cb_peek_front(&UART0_RX_Buffer, &val); 
	return val;
}


int USART0_Available() 
{
	return cb_size(&UART0_RX_Buffer); 
}


void USART0_Flush() 
{
	char temp; 
	while (cb_size(&UART0_RX_Buffer)>0) 
	{
		cb_pop_front(&UART0_RX_Buffer, &temp); 
	}
}





ISR(USART_UDRE_vect)
/*************************************************************************
Function: UART Data Register Empty interrupt
Purpose:  called when the UART is ready to transmit the next byte
**************************************************************************/
{
	if (cb_size(&UART0_TX_Buffer) > 0)
	{
		char temp; 
		cb_pop_front(&UART0_TX_Buffer, &temp);
		UDR0 = temp; 
	}
	else
	{
		UCSR0B &=  ~(1<<UDRIE0); //disable sending when buffer empty
	}
}



char received0;
ISR(USART_RX_vect) {
	received0 = UDR0; // might be able to eliminate this line.
	cb_push_back(&UART0_RX_Buffer, &received0);
}