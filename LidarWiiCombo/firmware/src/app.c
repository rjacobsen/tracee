/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/



// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include <stdbool.h>
#include "system_definitions.h"
#include "app.h"
#include "uart_handler.h"
#include "FastTransfer.h"
#include "FastTransfer1.h"
#include "MultiLIDAR.h"
#include "LidarDecoder.h"
#include "LidarDataInterpreter.h"
// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;

    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}

#define MASTER
/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{

    /* Check the application's current state. */
   switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            bool appInitialized = true;
            currentOC_val = 1800;
            FastTransTestCounter = 0;
            if(COMPUT_AUTO_DEMENTIONS == false)
            {
                Generate_LookUpTable();
            }
            
            else
            {
                long sumR = 0;
                long sumL = 0;
                int inc;
                for(inc = 0; inc < 30;inc++)
                {
                    decode_LidarData();
                    sumL+=distanceReading[180];
                    sumR+=distanceReading[0];
                }
                sumR = sumR / 30;
                sumL = sumL / 30;
                LeftSideDistance = sumL;
                RightSideDistance = sumR;
                Generate_LookUpTable();                 
            }
            
            
#ifndef MASTER
//            if(PORTFbits.RF5 == 0)
//            {
                LED5 = ON;
                isMaster = false;
                initFTLIDARPair(isMaster); 
#else             
//            }            
//            else
//            {
                LED5 = OFF;
                isMaster = true;
                initFTLIDARPair(isMaster);
#endif
//            }
                
             //Currently remapped to the CAN header
             begin(receiveArray, sizeof(receiveArray),FastTransferAddress,false, Send_put_2,Receive_get_2,Receive_available_2,Receive_peek_2);
           
           
           // LED4 = ON;
            if (appInitialized)
            {
                LED2 = ON;
                DRV_OCO_Change_PulseWidth(1800);
                appData.state = APP_STATE_VERIFIY_LidarSPEED;
            }
            break;
        }

        case APP_STATE_SERVICE_TASKS:
        {
            appData.state = APP_STATE_LIDAR;
            break;
        }
        case APP_STATE_VERIFIY_LidarSPEED:
        {
            int newOC_val = currentOC_val;
//            unsigned short _speed = Get_LidarSpeed();
//
//            while(!((_speed < LIDAR_SPEED_UPPER_BOUND) && (_speed > LIDAR_SPEED_LOWER_BOUND)))
//            {            
//
//                    TimerTick();
//                    if(_speed > LIDAR_SPEED_UPPER_BOUND) 
//                    {
//                        newOC_val -= LIDAR_PULSE_WIDTH_INCREMENT_SIZE;
//                        DRV_OCO_Change_PulseWidth(newOC_val);
//                        //LED5 ^= 5;
//                    }
//                    if(_speed < LIDAR_SPEED_LOWER_BOUND) 
//                    {
//                         newOC_val += LIDAR_PULSE_WIDTH_INCREMENT_SIZE;
//                         DRV_OCO_Change_PulseWidth(newOC_val);
//                         //LED4 ^= 5;
//                    }
//                    GLOBAL_100ms_TIMER_FLAG = false;
//                    while(GLOBAL_100ms_TIMER_FLAG == false)
//                    {
//                        TimerTick();
//                    }//wait for the motors to spin up or down
//                    _speed = Get_LidarSpeed();
//            }
//            currentOC_val = newOC_val;
            appData.state = APP_STATE_LIDAR;
            break;
        }
       
 
        case APP_STATE_LIDAR:                              
        {
            while( appData.state == APP_STATE_LIDAR)
            {
                TimerTick();
               
                receiveData();
                
                //DRV_USART2_WriteByte(0x00);
                decode_LidarData();
                startObjectDetection();                
                
                TimerTick();
                //for continuous transmission of the objects data
                if((GLOBAL_100ms_TIMER_FLAG == true)) 
                {
//                    #ifdef SEND_DATA_TO_DEBUGGER
//                        SendOBJData_debug();
//                    #else       
                        if(ContinuousTranmission == true)
                        {
                            switch(TransmissionMode)
                            {
                                case FASTTRANS_RX_CONTINUOUS_LEAPFROG_OBJ_CAPTURE:
                                    LEAPFROG_OBJ_CAPTURE();
                                    break;
                                case FASTTRANS_RX_CONTINUOUS_OBJ_CAPTURE:
                                    //Sends the object's angle and magnitude
                                    transmitOBJ_Data(true, false, false, true);
                                    break;
                            }
                        }
//                    #endif
                    GLOBAL_100ms_TIMER_FLAG = false;
                   // appData.state = APP_STATE_VERIFIY_LidarSPEED;
                }
            }
           
            break;
        }
       case FASTTRANS_TEST:
       {
            if((GLOBAL_100ms_TIMER_FLAG == true)) 
            { 
                ToSend(0,0xFFFF,  &OutGoing_DataTransBuff);
                sendData(FastTransTestCounter, &OutGoing_DataTransBuff);
                
                GLOBAL_100ms_TIMER_FLAG = false;
                if(FastTransTestCounter > 10)
                {
                    FastTransTestCounter = 0;
                }
            }
           break;
       }
        /* TODO: implement your application state machine.*/
        

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}
void TimerTick()
{
    if(TimerTickFlag)
    {      

//        if((TimerCounter / 10) >= 0)
//        {
//            GLOBAL_10ms_TIMER_FLAG = true;

//            if((TimerCounter / 50) >= 0)
//            {
//               GLOBAL_50ms_TIMER_FLAG = true;

            if((TimerCounter / 10) >= 1)
            {
                GLOBAL_100ms_TIMER_FLAG = true;
                TimerCounter = 0;
                if(isMaster == false )
                {
                    if(GLOBAL_100ms_TIMER_FLAG == true)
                    {
                        LED3 ^= 1;
                        sendLIDARObject(0,0, Get_objAngle(), Get_objMag());
                    }
                }
                else
                {
                    //LED5 ^= 1;
                    receiveLIDARData();
                    SendOBJData_debug();
                }
            }
//            }
    //        if((TimerCounter % 20) == 0)
    //        {
    //            GLOBAL_20ms_TIMER_FLAG = true;  
    //        }
//        }
//        if(TimerCounter >= 1000)
//        {
//            GLOBAL_1000ms_TIMER_FLAG = true;
//            TimerCounter = 0;
//
//        }

        TimerTickFlag = false;
    }
}

 

/*******************************************************************************
 End of File
 */
