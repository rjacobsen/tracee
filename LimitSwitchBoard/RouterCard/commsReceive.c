/*
 * commsReceive.c
 *
 * Created: 4/26/2017 2:22:24 PM
 *  Author: Zac
 */ 

#include "commsReceive.h"
#include <stdlib.h>
#include <avr/io.h>
#include "FastTransfer.h"
#include "Timer.h"
#include "LEDs.h"
#include "Motor.h"
#include "CommsDefenition.h"
#include "CANFastTransfer.h"

int macroCommand=0, macroSubCommand=0;
int MotorCommand, MotorValue;
timer_t sendBackTimer, commsTimer, safetyTimer, mouseGyroTimer;
bool macroActive = false;
bool readyToSend=false;
int prevLeftMotorCommand, prevRightMotorCommand, prevBucketCommand, prevConveyorCommand;

int getMacroCommand(void)
{
	return macroCommand;
}

int getMacroSubCommand(void)
{
	return macroSubCommand;
}

void parseComms(void)
{
	int * receiveArrayAdd	=getReceiveArray1();
	MotorCommand		=receiveArrayAdd[CommandStyle];
	MotorValue			=receiveArrayAdd[CommandValue];
}


void setupCommsTimers(void)
{
	setTimerInterval(&commsTimer,10);
	setTimerInterval(&safetyTimer,500);	
}

int multiSend=0;

void updateComms(void)
{
		
		if(timerDone(&commsTimer)) //Check the comms when the timer says to
		{		
			if(ReceiveDataCAN()) {
				if(getMotorCommand()==VelocityMode) {
					if(getMotorValue>0) {
						//turn on right pin, turn off left pin
						PORTC = (1<<RightPin) | (0<<LeftPin); 
						//set pulse width to absolute value
						setOC1BPulseWidth(getMotorValue());	
					}
					else if(getMotorValue()==0) {
						//stop motor
						//turn of both left and right pins
						PORTC = (0<<RightPin) | (0<<LeftPin); 
						//turning off both pins should have been enough
						//but to be safe, set pulse width to 0.
						setOC1BPulseWidth(0);	
					}
					else {
						//value is negative
						//turn on left pin, turn off right pin
						PORTC = (0<<RightPin) | (1<<LeftPin); 
						//set pulse width to absolute value
						setOC1BPulseWidth((-1*getMotorValue()));	
					}
					
				}
				readyToSend=true;
				resetTimer(&safetyTimer);
			}
			resetTimer(&commsTimer);
		}
		
		if(timerDone(&safetyTimer))
		{
			//stop motor
			//turn of both left and right pins
			PORTC = (0<<RightPin) | (0<<LeftPin);
			//turning off both pins should have been enough
			//but to be safe, set pulse width to 0.
			setOC1BPulseWidth(0);
			
		}
}

int getMotorCommand(void)
{
	return MotorCommand;
}

int getMotorValue(void)
{
	return MotorValue;
}
