/*
 * UART_handler.h
 *
 * Created: 5/18/2017 2:59:35 PM
 *  Author: Zac
 */ 


#ifndef UART_HANDLER_H_
#define UART_HANDLER_H_

void USART_Init( unsigned long baud) ;
void SendChar(char data);



#endif /* UART_HANDLER_H_ */