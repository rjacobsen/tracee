/*
 * commsReceive.h
 *
 * Created: 4/26/2017 2:22:33 PM
 *  Author: Zac
 */ 


#ifndef COMMSRECEIVE_H_
#define COMMSRECEIVE_H_

//receive array definitions.
#define LASTBOARD		0
#define CommandStyle 1
	#define VelocityMode 1
	#define PositionMode 2
#define CommandValue 2

#define LeftPin 5
#define RightPin 6

#include <stdbool.h>

bool manualMode(void);
void setupCommsTimers(void);
void updateComms(void);
void parseComms(void);
int getMotorCommand(void);
int getMotorValue(void);


#endif /* COMMSRECEIVE_H_ */