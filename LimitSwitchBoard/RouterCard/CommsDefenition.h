//This file contains receive array defenitions for all processors
//universal
#define LastBoardReceived 0 

//For UART-to-CAN code, doesn't include address of Router card itself, since it is the end point
//and isn't passing through data. 
	//For Router so it can forward data to UART
	#define MINUARTADRESS 1
	#define MAXUARTADDRESS 3
	//For Router so it can forward data to CAN bus
	#define MINCANADDRESS 5
	#define MAXCANADDRESS 10

//1 Control Box
#define ControlBoxAddress 1

//2 Router Card
#define RouterCardAddress 4

//3 Navigation PIC
#define NavigationAddress 5

//4 Beacon
#define BeaconAddress 2

//5 Sensor PIC
#define SensorAddress 6

//6 Mouse and Gyro PIC
#define MouseGyroAddress 7

//7 Electro Statics/Bucket Shaker
#define BucketAddress 8

//8 Debugger 
#define DebuggerAddress 3

//9 Left Bumper Board
#define LeftBumperAddress 9

//10 Right Bumper Board
#define RightBumperAddress 10

//TEMPORARY FAST TRANSFER ADDRESS

#define MotorControllerAddress 6