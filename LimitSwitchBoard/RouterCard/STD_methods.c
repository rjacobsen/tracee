/*
 * STD_Methods.c
 *
 * Created: 5/14/2017 5:37:09 PM
 *  Author: Seth Carpenter
 */ 
#include "STD_Methods.h"
#include <stdlib.h>


bool isInRange(float newVal, float Reference, float deltaRange)
{
	//if newVal is with in + or - deltaRange of the Reference variable
	if(abs((int)Reference - (int)newVal) > deltaRange)
	{
		//not in range
		return false;
	}
	//yah! we are in range
	return true;
}
