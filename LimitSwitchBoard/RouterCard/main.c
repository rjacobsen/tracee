/*
 * RouterCard.c
 *
 * Created: 4/13/2016 11:28:41 PM
 * Author : reed
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000
#include <util/delay.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "can.h"
#include "..\..\Common\CommsDefenition.h"
#include "general.h"
#include "assert.h"
#include "CANFastTransfer.h"
#include "commsReceive.h"
#include "LEDs.h"
#include "Config.h"
#include "Motor.h"
//#include "InputButtons.h"
#include "Timer.h"
#include "macros.h"
#include "UART_handler.h"


//Method to use button inputs to command the motors for testing without comms

 

//Time keeps for comms management
timer_t ledTimer,  checkCANTimer;


int main(void) 
{
	sei();
	
	//Startup the USARTs
	//USART0_Init(115200); //on programming line, and no real need for it if not debugging. 
	 
	//Setup the I/O ports
	PORTD	= 0b11100000;//D7-D5 are debug LEDs, D4 is the button input.
	DDRD	= 0b11100000; 
	//TODO: We need to make sure that we are not setting the portB and DDRB registers for pb2 as output when trying to implement the bumper switches
	PORTB	= 0b00000100;//enable pull up for bumper switch
	DDRB	= 0b00000100; //save PB2 for switch input
	DDRC = 0b01110010; //C5 & C6 for left right control, C1 for PWM 
	PORTC = 0b01000000;
	////if the pd1 is low then the device ID should be LEFT Board
	if(((PIND & (1 << PD1)) == 0))
	{
		moduleAddressCAN = BumperSwitchLeft;
	}
	else
	{
		moduleAddressCAN = BumperSwitchRight;
	}
	//moduleAddressCAN = BumperSwitchLeft;

	for(int i=0; i <= NUMBER_OF_LEDS; i++)
	{
		setLED(i,ON);
		_delay_ms(100);
	}
	
	for(int i=0; i <= NUMBER_OF_LEDS; i++)
	{
		setLED(i,OFF);
		_delay_ms(100);
	}
	
	//Initialize the timer0
	initTimer0();
	initTimer1(); 
	setOC1BPulseWidth(0);
	
	//Init the CAN here
	can_init();
	
	//Start communications handlers
	initCANFastTransfer();
	//USART_Init(115200);
	initMotors();
	
	setTimerInterval(&ledTimer,1000);
	setTimerInterval(&checkCANTimer,50);
	//setupCommsTimers();
// 	ToSendCAN(1, 2);
// 	sendDataCAN(MouseGyroAddress);

	initializeExternalInterrupts();
	//setMotorVelocity(10);
	//static int counter=0;
	//while(counter<500)
	//{
		//updateMotorRamping();
		//_delay_ms(5);
		//counter++;
	//}
	//setMotorVelocity(0);
	while(1) 
	{	 
		
		//update the can buffer
		checkCANFTdata();
		
		//see if there is a command that is greater then zero(not a stop command)
		if(getPerformNavigationCommand()>0)
		{
			
			doNavigationCommand();
		}
		else
		{
			stopNavigationCommand();
		}
		
		
		if(timerDone(&ledTimer))	
		{
			toggleLED(LED1);
		}

		//if (timerDone(&checkCANTimer)) 
		//{
	//
		//}
		
		//motor control handled in here
		updateMotorRamping();
		
		_delay_us(500);
		 
	
	}
}


