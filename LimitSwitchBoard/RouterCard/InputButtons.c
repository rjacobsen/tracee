/*
 * InputButtons.c
 *
 * Created: 5/4/2017 9:57:10 AM
 *  Author: Zac
 */ 
#include "InputButtons.h"
#include <avr/io.h>


bool buttonPressed(buttonList_t b)
{
	switch(b)
	{
		case SPECIAL:
			return (!(PINC & (1<<PINC0)));
			break;
		case DWN:
			return (!(PINC & (1<<PINC1)));
			break;
		case UP:
			return (!(PINC & (1<<PINC2)));
			break;
		case BCK:
			return (!(PINA & (1<<PINA7)));			
			break;
	}
	return false;
}