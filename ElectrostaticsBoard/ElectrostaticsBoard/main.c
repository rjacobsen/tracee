/*
 * ElectrostaticsBoard.c
 *
 * Created: 5/23/2017 9:41:20 AM
 * Author : Zac
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000

#include <util/delay.h>
#include <stdbool.h>
#include "can.h"
#include "CANFastTransfer.h"


bool trig1, trig2;
int main(void)
{
	DDRD  = 0b11110000;
	DDRB  = 0b00010100;	//B4 - 12V on off  B5 - sol
	DDRC  = 0b11001110;
	PORTC = 0b00000110;
	PORTD = 0b00000000;
	
	PORTB = 0b00001000;
	

	//
	//initCANFastTransfer();
	//_delay_ms(500);
	//
	//ToSendCAN(1,0);
	//sendDataCAN(12);
    /* Replace with your application code */
    while (1) 
    {
		
		//checkCANFTdata();
		
		if(PORTC & 0b010000000 && trig1)
		{
			trig1=false;
			PORTC ^= 0b10000000;
			
		}
		else
		{
			trig1=true;
			PORTC ^= 0b10000000;
		}
		if(PORTB & 0x08 && trig2)
		{
			trig2=false;
			PORTC ^= 0b10000000;
			
		}
		else
		{
			trig2=true;
			PORTC ^= 0b10000000;
			
		}
		PORTC^=0b01000000;
		_delay_ms(500);
    }
}

